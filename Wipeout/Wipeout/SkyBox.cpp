#include "SkyBox.h"


SkyBox::SkyBox(float X, float Y, float Z, const char * PosXFilename, const char * NegXFilename, const char * PosYFilename, const char * NegYFilename, const char * PosZFilename, const char * NegZFilename)
{
	this->pos = vec3(X, Y, Z);
	T = mat4::translationMatrix(X, Y, Z);
	projection = mat4::perspectiveProjection(90.0f, 1.0f, 0.1f, 100.0f);
	float skyboxVertices[] = {
		// positions          
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		-1.0f,  1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f,  1.0f
	};
	Texture = new CubemapTexture( PosXFilename, NegXFilename, PosYFilename, NegYFilename, PosZFilename, NegZFilename);


	// Setup single VAO / VBO for both elements 
	glGenVertexArrays(1, &skyboxVAO);
	glBindVertexArray(skyboxVAO);

	// Setup VBO
	glGenBuffers(1, &skyboxVBO);
	glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), skyboxVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);

	// Unbind VAO for now
	glBindVertexArray(0);
	// Setup shader
	Shader = setupShaders(std::string("Shaders\\skybox.vs.txt"), std::string("Shaders\\skybox.fs.txt"));
	locP = glGetUniformLocation(Shader, "projection");
	locV = glGetUniformLocation(Shader, "view");
	locT = glGetUniformLocation(Shader, "T");

	
}

SkyBox::~SkyBox()
{
}

void SkyBox::Render()
{
	view = glm::mat4(glm::mat3(view));
	glDepthFunc(GL_LEQUAL);
	glUseProgram(Shader);
	glUniformMatrix4fv(locP, 1, GL_FALSE, (GLfloat*)&projection);
	glUniformMatrix4fv(locV, 1, GL_FALSE, (GLfloat*)&view);
	glUniformMatrix4fv(locT, 1, GL_FALSE, (GLfloat*)&T);
	glBindVertexArray(skyboxVAO);
	glBindTexture(GL_TEXTURE_CUBE_MAP, Texture->textureID);
	
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glDepthFunc(GL_LESS);
	
	
}
