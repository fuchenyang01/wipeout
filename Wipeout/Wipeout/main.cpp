
#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <glew\glew.h>
#include <freeglut\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include"vehicle.h"
#include"NPC.h"
#include <iostream>
#include"Shield.h"
#include"SkyBox.h"
#include<al.h>
#include<alc.h>
#include"GURiffModel.h"
#include<mmreg.h>

using namespace std;
using namespace CoreStructures;

// Globals
ALCdevice* alcDevice = alcOpenDevice(NULL);
ALCcontext* alcContext = nullptr;

GUClock *gameClock = nullptr;
Vehicle *vehicle = nullptr;
Shield *shield = nullptr;
NPC *npc = nullptr;
NPC *N = nullptr;
NPC *road = nullptr;
SkyBox *skybox = nullptr;
ALuint buffer1 = 0;
ALuint source1 = 0;
ALuint buffer2 = 0;
ALuint source2 = 0;

// Function Prototypes
void init(int argc, char* argv[]);
void update(void);
void display(void);
void reportContextVersion(void);
void reportExtensions(void);
void keyDown(unsigned char key, int x, int y);


#pragma region Helper functions

BOOL isKeyPressed(int key) {

	return GetAsyncKeyState(key) & 0x80000000;
}

bool isPlaying(ALuint source)
{
	ALenum state;

	alGetSourcei(source, AL_SOURCE_STATE, &state);

	return (state == AL_PLAYING);
}

int main(int argc, char* argv[]) {

	init(argc, argv);
	glutMainLoop();

	return 0;
}



void init(int argc, char* argv[]) {

	// Initialise FreeGLUT
	glutInit(&argc, argv);

	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);

	glutInitWindowSize(800, 800);
	glutInitWindowPosition(64, 64);
	glutCreateWindow("Wipeout");
	reportContextVersion();
	reportExtensions();
	// Register callback functions
	//glutDisplayFunc(display);
	glutIdleFunc(update);
	// Key down handler
	glutKeyboardFunc(keyDown); 
	// Initialise GLEW library
	glewInit();

	// Initialise main clock
	gameClock = new GUClock();

	// Initialise OpenGL...

	// Setup colour to clear the window
	glClearColor(0.3f, 0.3f, 0.3f, 0.0f);
	glLineWidth(1.0f);

	vehicle=new Vehicle(0.0f, 0.0f, 0.0f, 0,"Shaders\\vehicle.vs.txt", "Shaders\\vehicle.fs.txt", "bitmap2.png",string("ship2.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);
	
	npc = new NPC(0.7,0,-3.0f, 0, "Shaders\\NPC.vs.txt", "Shaders\\NPC.fs.txt", "beast_texture.bmp", string("beast.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	road = new NPC(0, 0, 0, 0,"Shaders\\NPC.vs.txt", "Shaders\\NPC.fs.txt", "Textures.jpg", string("map.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	N = new NPC(0, 0, 0, 0, "Shaders\\NPC.vs.txt", "Shaders\\NPC.fs.txt", "npc.png", string("npc.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);
	skybox = new SkyBox(0.f, 0.f, 0.f, "rt.tga", "lf.tga", "up.tga", "dn.tga", "bk.tga", "ft.tga");

	shield = new Shield(vehicle->getPos().x, vehicle->getPos().y, vehicle->getPos().z, vehicle->getTheta(), "Shaders\\shield.vs.txt", "Shaders\\shield.fs.txt");
	glEnable(GL_DEPTH_TEST);

	//Setting up OpenAL��
	alcContext = alcCreateContext(alcDevice, NULL);
	alcMakeContextCurrent(alcContext);
	
	alGenBuffers(1, &buffer1);
	auto mySoundData = new GURiffModel("engine_01.wav");

	RiffChunk formatChunk = mySoundData->riffChunkForKey(' tmf');
	RiffChunk dataChunk = mySoundData->riffChunkForKey('atad');

	WAVEFORMATEXTENSIBLE wv;
	memcpy_s(&wv, sizeof(WAVEFORMATEXTENSIBLE), formatChunk.data, formatChunk.chunkSize);

	// Create the buffer �C here we hardcode AL_FORMAT_MONO16 for the format
	alBufferData(
		buffer1,
		AL_FORMAT_STEREO16,
		(ALvoid*)dataChunk.data,
		(ALsizei)dataChunk.chunkSize,
		(ALsizei)wv.Format.nSamplesPerSec
	);

	alGenSources(1, &source1);

	// Attach buffer1 to source1
	alSourcei(source1, AL_BUFFER, buffer1);
	// Setup source1 location, velocity and direction
	alSource3f(source1, AL_POSITION, pos.x, pos.y, pos.z);
	alSource3f(source1, AL_VELOCITY, 0.0f, 0.0f, 0.0f);
	alSource3f(source1, AL_DIRECTION, 0.0f, 0.0f, 0.0f);

	auto cameraLocation = Position;
	ALfloat listenerVel[] = { 0.0f, 0.0f, 0.0f };
	ALfloat listenerOri[] = { -cameraLocation.x, -cameraLocation.y, -cameraLocation.z, 0.0f, 1.0f, 0.0f };
	alListenerfv(AL_POSITION, (ALfloat*)(&cameraLocation));
	alListenerfv(AL_VELOCITY, listenerVel);
	alListenerfv(AL_ORIENTATION, listenerOri);
	//set bgm
	alGenBuffers(1, &buffer2);
	auto bgm = new GURiffModel("bgm.wav");
	RiffChunk formatChunk2 = bgm->riffChunkForKey(' tmf');
	RiffChunk dataChunk2 = bgm->riffChunkForKey('atad');
	WAVEFORMATEXTENSIBLE wv2;
	memcpy_s(&wv2, sizeof(WAVEFORMATEXTENSIBLE), formatChunk2.data, formatChunk2.chunkSize);
	alBufferData(
		buffer2,
		AL_FORMAT_MONO16,
		(ALvoid*)dataChunk2.data,
		(ALsizei)dataChunk2.chunkSize,
		(ALsizei)wv2.Format.nSamplesPerSec
	);
	alGenSources(1, &source2);
	// Attach buffer2 to source2
	alSourcei(source2, AL_BUFFER, buffer2);

	// Setup source2 location, velocity and direction
	alSource3f(source2, AL_POSITION, 0.7f, 0.0f, -3.0f);
	alSource3f(source2, AL_VELOCITY, 0.0f, 0.0f, 0.0f);
	alSource3f(source2, AL_DIRECTION, 0.0f, 0.0f, 0.0f);

	ALfloat listenerVel2[] = { 0.0f, 0.0f, 0.0f };
	ALfloat listenerOri2[] = { 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f };

	alListenerfv(AL_POSITION, (ALfloat*)(&cameraLocation));
	alListenerfv(AL_VELOCITY, listenerVel2);
	alListenerfv(AL_ORIENTATION, listenerOri2);


	
}

// Update function
void update(void) {
	//play bgm
	auto cameraLocation =Position;
	alListenerfv(AL_POSITION, (ALfloat*)(&cameraLocation));
	ALfloat orientation2[] = { -cameraLocation.x, -cameraLocation.y, -cameraLocation.z, 0.0f, 1.0f, 0.0f };
	alListenerfv(AL_POSITION, (ALfloat*)(&cameraLocation));
	alListenerfv(AL_ORIENTATION, orientation2);

	if (isPlaying(source2) == false) {
		alSourcePlay(source2);
	}
	// Update clock
	gameClock->tick();

	float tDelta = float(gameClock->gameTimeDelta());

	if (isKeyPressed(0x57)) {

		vehicle->move(tDelta, -1.0f);
		if (isPlaying(source1) == false) {
			alSourcePlay(source1);
		}	
	}
	if (isKeyPressed(0x57) == false) {
		alSourceStop(source1);
	}
	if (isKeyPressed(0x53)) {

		vehicle->move(tDelta, 1.0f);
	}

	if (isKeyPressed(VK_NUMPAD4)) {

		vehicle->rotate(tDelta, 1.0f);
		
	}
	else if (isKeyPressed(VK_NUMPAD6)) {

		vehicle->rotate(tDelta, -1.0f);
		
	}

	 if (isKeyPressed(VK_NUMPAD5)) {

		vehicle->updown(tDelta, 1.0f);

	}
	 else if (isKeyPressed(VK_NUMPAD8))
	 {
		 vehicle->updown(tDelta, -1.0f);
	 }

	 if (isKeyPressed(0x41)) {

		 vehicle->horizontal(tDelta, -1.0f);

	 }
	 else if (isKeyPressed(0x44))
	 {
		 vehicle->horizontal(tDelta, 1.0f);
	 }
	
	 if (isKeyPressed(VK_SPACE)) {

		 vehicle->move(tDelta, -2.0f);
	 }

	vehicle->rotate(tDelta, 0.0f);
	vehicle->updown(tDelta, 0.0f);
	
	
	// Update the window title to show current frames-per-second and seconds-per-frame data
	char timingString[256];
	sprintf_s(timingString, 256, "Wipeout. Average fps: %.0f; Average spf: %f", gameClock->averageFPS(), gameClock->averageSPF() / 1000.0f);
	glutSetWindowTitle(timingString);
	
	// Redraw the screen
	display();
}


// Main rendering functions
void display(void)
{
	//  Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
	//  Re-instate fixed function (OpenGL's default) shaders and clear previous transformations
	glUseProgram(0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	

	// Draw the vehicle
	vehicle->render();
	glUseProgram(0);
	npc->render();
	N->render();
	road->render();

	if (isKeyPressed(VK_SPACE)) {
		shield->setPos(vehicle->getPos());
		shield->setTheta(vehicle->getTheta());
		shield->render();
		
	}
	
	skybox->Render();

	
	
	
	//  Present the new frame to the screen

	glutSwapBuffers();
}


#pragma region Helper Functions

void reportContextVersion(void) {

	int majorVersion, minorVersion;

	glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
	glGetIntegerv(GL_MINOR_VERSION, &minorVersion);

	cout << "OpenGL version " << majorVersion << "." << minorVersion << "\n\n";
}

void reportExtensions(void) {

	cout << "Extensions supported...\n\n";

	const char *glExtensionString = (const char *)glGetString(GL_EXTENSIONS);

	char *strEnd = (char*)glExtensionString + strlen(glExtensionString);
	char *sptr = (char*)glExtensionString;

	while (sptr < strEnd) {

		int slen = (int)strcspn(sptr, " ");
		printf("%.*s\n", slen, sptr);
		sptr += slen + 1;
	}
}

void keyDown(unsigned char key, int x, int y) {

	// Toggle fullscreen (This does not adjust the display mode however!)
	if (key == 'f')
		glutFullScreenToggle();


}
