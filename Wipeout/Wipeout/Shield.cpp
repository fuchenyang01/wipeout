#include "Shield.h"






Shield::Shield(float X, float Y, float Z, float Theta, char * vsfilename, char * fsfilename)
{
	projection = mat4::perspectiveProjection(60.0f, 1.0f, 0.1f, 100.0f);
	this->pos = vec3(X, Y, Z);
	this->Theta = Theta;
	T = mat4::translationMatrix(X, Y, Z);
	R = mat4::rotationMatrix(0.0f, Theta, 0.0f);
	TR = T  * R;

	float Vertices[] = {
		// positions          
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		-1.0f,  1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f,  1.0f
	};
	// Setup single VAO / VBO for both elements 
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	// Setup VBO
	glGenBuffers(1, &PosVBO);
	glBindBuffer(GL_ARRAY_BUFFER, PosVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);

	// Unbind VAO for now
	glBindVertexArray(0);

	// Setup shader
	Shader = setupShaders(std::string(vsfilename), std::string(fsfilename));

	locP = glGetUniformLocation(Shader, "P");
	locT = glGetUniformLocation(Shader, "T");
	locV = glGetUniformLocation(Shader, "view");
	
}

Shield::~Shield()
{
}

void Shield::render(void)
{
	T = mat4::translationMatrix(pos.x, pos.y, pos.z);
	R = mat4::rotationMatrix(0.0f, Theta, 0.0f);
	TR = T  * R;
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);

	glUseProgram(Shader);

	glUniformMatrix4fv(locP, 1, GL_FALSE, (GLfloat*)&projection);
	glUniformMatrix4fv(locV, 1, GL_FALSE, (GLfloat*)&view);
	glUniformMatrix4fv(locT, 1, GL_FALSE, (GLfloat*)&TR);
	
	glBindVertexArray(VAO);

	glDrawArrays(GL_TRIANGLES, 0, 36);
	// Restore
	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);

}

void Shield::setPos(vec3 p)
{
	 pos = p;
}

void Shield::setTheta(float t)
{
	Theta = t;
}
