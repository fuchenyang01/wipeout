#pragma once

#include <Importer.hpp>
#include <scene.h>
#include <postprocess.h>  
#include <CoreStructures/GUPivotCamera.h>
#include <string>
static Assimp::Importer				importer;
class Scene
{
	const aiScene* scene;
public:
	Scene(const std::string& filename, unsigned int flags);
	~Scene();

	const aiScene *aiImportModel(const std::string& filename, unsigned int flags);
	void aiReportScene(const aiScene* scene);
	const aiScene* getScene();
};

