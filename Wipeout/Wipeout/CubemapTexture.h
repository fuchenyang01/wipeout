#pragma once

#include"vehicle.h"
using namespace std;

class CubemapTexture
{

public:
	CubemapTexture(
		const char * PosXFilename,
		const char * NegXFilename,
		const char * PosYFilename,
		const char * NegYFilename,
		const char * PosZFilename,
		const char * NegZFilename);
	~CubemapTexture();
	bool Load(const TextureProperties& properties);
	void Bind(GLenum TextureUnit);
	GLuint textureID;
private:
	const char * m_fileNames[6];
	
};

