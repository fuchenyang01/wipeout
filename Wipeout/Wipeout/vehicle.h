#pragma once

#include <glew/glew.h>
#include <string>
#include <CoreStructures\CoreStructures.h>
#include "shader_setup.h"
#include <FreeImage\FreeImagePlus.h>
#include "Scene.h"
#include"Camera.h"
#include <iostream>
#include"texture_loader.h"
// Alias to keep code more succinct
using namespace std;
using vec4 = CoreStructures::GUVector4;
using vec3 = CoreStructures::GUVector3;
using vec2 = CoreStructures::GUVector2;
using mat4 = CoreStructures::GUMatrix4;
__declspec(selectany) glm::mat4 view;
__declspec(selectany) glm::vec3 Position;
// Vehicle position
__declspec(selectany) vec3 pos;
class Vehicle
{

private:
	Scene* scene;
	Camera *camera = nullptr;
	vec4			p;
	// Vehicle velocity (scalar value - we'll use orientation to determine direction)
	// Distance per-second (constant for this demo)
	 float		velocity ;

	// Vehicle orientation (in radians)
	float			Theta =0;
	float			Thetaz = 0;
	float           Thetax = 0;
	glm::vec3 Target;
	glm::vec3 Up;


	GLuint			 PosVBO, IndicesVBO, TexCoordVBO, NormalsVBO, Shader, locT, locP,locV, locViewPos;
	GLuint			Texture;
	GLuint numMeshes;
	// Derived values - matrices based on position and orientation
	mat4			T, R, Rz, Rx,  TR; // T (translation); R (rotation)

	mat4 projection; 

	GLuint *VAO= new GLuint[numMeshes];
	GLuint *numVectics = new GLuint[numMeshes];
	GLuint *numfaces = new GLuint[numMeshes];
	void calculateDerivedMatices(void); 

public:
	
	Vehicle(float X, float Y, float Z, float Theta, char* vsfilename, char* fsfilename, char* Tfilename, const std::string& filename, unsigned int flags);



	// Interaction methods
	// Move forward (direction = 1) or backward (direction = -1)
	
	void move(float tDelta, float direction);
	void updown(float tDelta, float direction);
	void horizontal(float tDelta, float direction);
	void rotate(float tDelta, float direction); 
	void render(void); 
	vec3 getPos();
	float getTheta();
};
