#pragma once
#include <CoreStructures\CoreStructures.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


class Camera
{
	glm::vec3 Position;
	glm::vec3 Target;
	glm::vec3 Up;
	glm::mat4 view;
public:
	Camera(glm::vec3 position, glm::vec3 target, glm::vec3 up);
	~Camera();
	glm::mat4 getView();
};

