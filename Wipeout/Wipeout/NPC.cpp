#include"NPC.h"
#include"vehicle.h"

NPC::NPC(float X, float Y, float Z, float Theta, char* vsfilename, char* fsfilename, char * Tfilename, const std::string & filename, unsigned int flags)
{
		projection = mat4::perspectiveProjection(90.0f, 1.0f, 0.1f, 1000000.0f);
		this->p = vec3(X, Y, Z);
		this->Theta = Theta;
		T = mat4::translationMatrix(X, Y, Z);
		R = mat4::rotationMatrix(0.0f, Theta, 0.0f);
		TR = T  * R;
		// Setup VAO and VBOs

		scene = new Scene(filename, flags);
		Texture = fiLoadTexture(Tfilename, TextureProperties(false));
		// Setup VAO and VBOs

		numMeshes = scene->getScene()->mNumMeshes;
		auto M = scene->getScene()->mMeshes;
		for (unsigned int m = 0; m < numMeshes; m++) {
			numVectics[m] = M[m]->mNumVertices;
			numfaces[m] = M[m]->mNumFaces;

			GLfloat *TextureCoords = new GLfloat[numVectics[m] * 2];
			GLfloat *vertexPos = new GLfloat[numVectics[m] * 3];
			GLfloat *normals = new GLfloat[numVectics[m] * 3];
			GLuint *VertexIndices = new GLuint[numfaces[m] * 3];



		
			for (int i = 0, j = 0; i < numVectics[m] * 3; i = i + 3, j++) {

				vertexPos[i] = M[m]->mVertices[j].x;
				vertexPos[i + 1] = M[m]->mVertices[j].y;
				vertexPos[i + 2] = M[m]->mVertices[j].z;

			}


			for (int i = 0, j = 0; i < numVectics[m] * 2; i = i + 2, j++) {

				TextureCoords[i] = M[m]->mTextureCoords[0][int(j)].x;
				TextureCoords[i + 1] = M[m]->mTextureCoords[0][int(j)].y;

			}



			for (int i = 0, j = 0; i < numfaces[m] * 3; i = i + 3, j++) {

				VertexIndices[i] = M[m]->mFaces[int(j)].mIndices[0];
				VertexIndices[i + 1] = M[m]->mFaces[int(j)].mIndices[1];
				VertexIndices[i + 2] = M[m]->mFaces[int(j)].mIndices[2];
			}

	
			for (int i = 0, j = 0; i < numVectics[m] * 3; i = i + 3, j++) {

				normals[i] = M[m]->mNormals[j].x;
				normals[i + 1] = M[m]->mNormals[j].y;
				normals[i + 2] = M[m]->mNormals[j].z;

			}

			// Setup single VAO / VBO for both elements 
			glGenVertexArrays(1, &VAO[m]);
			glBindVertexArray(VAO[m]);

			// Setup VBO
			glGenBuffers(1, &PosVBO);
			glBindBuffer(GL_ARRAY_BUFFER, PosVBO);
			glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*numVectics[m] * 3, vertexPos, GL_STATIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
			glEnableVertexAttribArray(0);

			
			glGenBuffers(1, &TexCoordVBO);
			glBindBuffer(GL_ARRAY_BUFFER, TexCoordVBO);
			glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*numVectics[m] * 2, TextureCoords, GL_STATIC_DRAW);
			glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
			glEnableVertexAttribArray(2);

			glGenBuffers(1, &NormalsVBO);
			glBindBuffer(GL_ARRAY_BUFFER, NormalsVBO);
			glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)* numVectics[m] * 3, normals, GL_STATIC_DRAW);
			glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
			glEnableVertexAttribArray(3);

			glGenBuffers(1, &IndicesVBO);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IndicesVBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint)*numfaces[m] * 3, VertexIndices, GL_STATIC_DRAW);
			// Unbind VAO for now
			glBindVertexArray(0);
		}

		// Setup shader
		Shader = setupShaders(std::string(vsfilename), std::string(fsfilename));

		locP = glGetUniformLocation(Shader, "P");
		locT = glGetUniformLocation(Shader, "T");
		locV = glGetUniformLocation(Shader, "view");
		locViewPos = glGetUniformLocation(Shader, "viewPos");
		locVehiclePos = glGetUniformLocation(Shader, "vehiclePos");
	}

NPC::~NPC()
{
}

void NPC::render(void)
{

	glBindTexture(GL_TEXTURE_2D, Texture);
	glEnable(GL_TEXTURE_2D);
	glUseProgram(Shader);

	glUniformMatrix4fv(locP, 1, GL_FALSE, (GLfloat*)&projection);
	glUniformMatrix4fv(locV, 1, GL_FALSE, (GLfloat*)&view);
	glUniformMatrix4fv(locT, 1, GL_FALSE, (GLfloat*)&TR);
	glUniform3fv(locViewPos, 1, (GLfloat*)&Position);
	glUniform3fv(locVehiclePos, 1, (GLfloat*)&pos);

	for (int m = 0; m < numMeshes; m++) {
		glBindVertexArray(VAO[m]);

		glDrawElements(GL_TRIANGLES, numfaces[m] * 3, GL_UNSIGNED_INT, (GLvoid*)0);
	}
}

	

