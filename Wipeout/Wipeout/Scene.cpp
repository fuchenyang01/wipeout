#include "Scene.h"
#include <glew/glew.h>
#include <iostream>
using namespace std;
using namespace CoreStructures;

Scene::Scene(const std::string& filename, unsigned int flags)
{
	scene = aiImportModel(filename, flags);
}


Scene::~Scene()
{
}

const aiScene * Scene::aiImportModel(const std::string & filename, unsigned int flags)
{
	scene = importer.ReadFile(filename, flags);
	if (!scene)
		cout << "Sorry, " << filename << " not found!" << endl;
	else
		cout << filename << " loaded okay!" << endl;
	return scene;
}

void Scene::aiReportScene(const aiScene * scene)
{
	auto numMeshes = scene->mNumMeshes;
	auto M = scene->mMeshes;

	for (unsigned int i = 0; i < numMeshes; ++i) {

		cout << "Mesh " << i << ":" << endl;

		auto mptr = M[i];

		cout << "num vertices = " << M[i]->mNumVertices << endl;
		cout << "num faces = " << M[i]->mNumFaces << endl;
	}
}

const aiScene * Scene::getScene()
{
	return scene;
}
