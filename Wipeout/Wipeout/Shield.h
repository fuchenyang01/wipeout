#pragma once
#include"vehicle.h"
class Shield
{
	GLuint			 VAO, PosVBO, Shader, locT, locP, locV;
	float			Theta = 0;
	vec3			pos;
	mat4 projection;
	mat4 T;
	mat4 R;
	mat4 TR;
public:
	Shield(float X, float Y, float Z, float Theta, char* vsfilename, char* fsfilename);
	~Shield();
	void render(void);
	void setPos(vec3 p);
	void setTheta(float t);


};

