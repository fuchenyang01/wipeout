#pragma once
#include <glew/glew.h>
#include <string>
#include <CoreStructures\CoreStructures.h>
#include "shader_setup.h"
#include <FreeImage\FreeImagePlus.h>
#include <Importer.hpp>
#include <scene.h>
#include <postprocess.h>
#include"Camera.h"
#include"Scene.h"
#include "texture_loader.h"


// Alias to keep code more succinct
using vec3 = CoreStructures::GUVector3;
using mat4 = CoreStructures::GUMatrix4;

class NPC
{
public:
	NPC(float X, float Y, float Z, float Theta, char* vsfilename, char* fsfilename, char* Tfilename, const std::string& filename, unsigned int flags);
	~NPC();
	void render(void);
	


private:
	Scene* scene;
	vec3			p;
	float			Theta = 0;
	GLuint			 PosVBO, IndicesVBO, TexCoordVBO, NormalsVBO ,Shader, locT, locP, locV, locViewPos, locVehiclePos;
	GLuint numMeshes;
	GLuint			Texture;
	GLuint *VAO = new GLuint[numMeshes];
	GLuint *numVectics = new GLuint[numMeshes];
	GLuint *numfaces = new GLuint[numMeshes];
	mat4 projection; 
	mat4 T; 
	mat4 R;
	mat4 TR; 
};

