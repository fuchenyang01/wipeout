#include "Camera.h"



Camera::Camera(glm::vec3 position, glm::vec3 target, glm::vec3 up)
{
	Position = position;
	Target = target;
	Up = up;

	view = glm::lookAt(Position, Target, Up);
	
}


Camera::~Camera()
{
}

glm::mat4 Camera::getView()
{
	return view;
}
