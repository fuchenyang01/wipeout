#include "CubemapTexture.h"





CubemapTexture::CubemapTexture(const char * PosXFilename, const char * NegXFilename, const char * PosYFilename, const char * NegYFilename, const char * PosZFilename, const char * NegZFilename)
{
	m_fileNames[0] = PosXFilename;
	m_fileNames[1] = NegXFilename;
	m_fileNames[2] = PosYFilename;
	m_fileNames[3] = NegYFilename;
	m_fileNames[4] = PosZFilename;
	m_fileNames[5] = NegZFilename;
	textureID = 0;
	Load(TextureProperties(false));
}

CubemapTexture::~CubemapTexture()
{
}

bool CubemapTexture::Load(const TextureProperties& properties)
{

	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	BOOL				fiOkay = FALSE;
	fipImage			I;

	for (unsigned int i = 0; i < 6; i++) {
		fiOkay = I.load(m_fileNames[i]);
		I.flipVertical();
		if (!fiOkay) {

			cout << "FreeImagePlus: Cannot open image file.\n";
			return 0;
		}

		if (properties.flipImageY) {

			fiOkay = I.flipVertical();
		}

		fiOkay = I.convertTo32Bits();

		if (!fiOkay) {

			cout << "FreeImagePlus: Conversion to 24 bits successful.\n";
			return 0;
		}

		auto w = I.getWidth();
		auto h = I.getHeight();
		BYTE *buffer = I.accessPixels();

		if (!buffer) {

			cout << "FreeImagePlus: Cannot access bitmap data.\n";
			return 0;
		}
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, w, h, 0, GL_BGRA, GL_UNSIGNED_BYTE, buffer);
		
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	return true;
}

void CubemapTexture::Bind(GLenum TextureUnit)
{
	glActiveTexture(TextureUnit);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
}
