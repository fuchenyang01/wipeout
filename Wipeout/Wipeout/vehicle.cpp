#include "vehicle.h"



void Vehicle::calculateDerivedMatices(void)
{
	T = mat4::translationMatrix(pos.x, pos.y, pos.z);

	R = mat4::rotationMatrix(0.0f, Theta, 0.0f);

	Rz = mat4::rotationMatrix(0.0f, 0.0f, Thetaz);

	Rx= mat4::rotationMatrix(Thetax, 0.0f, 0.0f);

	TR = T  * R * Rz * Rx;

}

Vehicle::Vehicle(float X, float Y, float Z, float Theta, char* vsfilename, char* fsfilename, char * Tfilename, const std::string & filename, unsigned int flags)
{
	projection = mat4::perspectiveProjection(60.0f, 1.0f, 0.1f, 100.0f);
	velocity = 1000.0f;
	pos = vec3(X, Y, Z);
	this->Theta = Theta;
	scene = new Scene(filename, flags);
	Texture = fiLoadTexture(Tfilename, TextureProperties(false));
	// Setup VAO and VBOs
	numMeshes = scene->getScene()->mNumMeshes;
	auto M = scene->getScene()->mMeshes;
	for (unsigned int m = 0; m < numMeshes; m++) {
		numVectics[m] = M[m]->mNumVertices;
		numfaces[m] = M[m]->mNumFaces;

		GLfloat *TextureCoords = new GLfloat[numVectics[m] * 2];
		GLfloat *vertexPos = new GLfloat[numVectics[m] * 3];
		GLfloat *normals = new GLfloat[numVectics[m] * 3];
		GLuint *VertexIndices = new GLuint[numfaces[m] * 3];



	
		for (int i = 0, j = 0; i < numVectics[m] * 3; i = i + 3, j++) {
			vertexPos[i] = M[m]->mVertices[j].x;
			vertexPos[i + 1] = M[m]->mVertices[j].y;
			vertexPos[i + 2] = M[m]->mVertices[j].z;
		}


		for (int i = 0, j = 0; i < numVectics[m] * 2; i = i + 2, j++) {

			TextureCoords[i] = M[m]->mTextureCoords[0][int(j)].x;
			TextureCoords[i + 1] = M[m]->mTextureCoords[0][int(j)].y;

		}



		for (int i = 0, j = 0; i < numfaces[m] * 3; i = i + 3, j++) {

			VertexIndices[i] = M[m]->mFaces[int(j)].mIndices[0];
			VertexIndices[i + 1] = M[m]->mFaces[int(j)].mIndices[1];
			VertexIndices[i + 2] = M[m]->mFaces[int(j)].mIndices[2];

		}


		for (int i = 0, j = 0; i < numVectics[m] * 3; i = i + 3, j++) {

			normals[i] = M[m]->mNormals[j].x;
			normals[i + 1] = M[m]->mNormals[j].y;
			normals[i + 2] = M[m]->mNormals[j].z;

		}
		// Setup single VAO / VBO for both elements 
		glGenVertexArrays(1, &VAO[m]);
		glBindVertexArray(VAO[m]);

		// Setup VBO
		glGenBuffers(1, &PosVBO);
		glBindBuffer(GL_ARRAY_BUFFER, PosVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)* numVectics[m] * 3, vertexPos, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
		glEnableVertexAttribArray(0);


		glGenBuffers(1, &TexCoordVBO);
		glBindBuffer(GL_ARRAY_BUFFER, TexCoordVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*numVectics[m] * 2, TextureCoords, GL_STATIC_DRAW);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
		glEnableVertexAttribArray(2);

		glGenBuffers(1, &NormalsVBO);
		glBindBuffer(GL_ARRAY_BUFFER, NormalsVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)* numVectics[m] * 3, normals, GL_STATIC_DRAW);
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
		glEnableVertexAttribArray(3);

		glGenBuffers(1, &IndicesVBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IndicesVBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint)*numfaces[m] * 3, VertexIndices, GL_STATIC_DRAW);
		// Unbind VAO for now
		glBindVertexArray(0);
	}



	// Setup shader
	Shader = setupShaders(std::string(vsfilename), std::string(fsfilename));

	locT = glGetUniformLocation(Shader, "T");
	locP = glGetUniformLocation(Shader, "P");
	locV = glGetUniformLocation(Shader, "view");
	locViewPos = glGetUniformLocation(Shader, "viewPos");
}

void Vehicle::move(float tDelta, float direction)
{
	pos += vec3(

		sinf(Theta) * direction *  tDelta * velocity,
		0.0f,
		cosf(Theta)* direction *  tDelta * velocity);
	calculateDerivedMatices();
}

void Vehicle::updown(float tDelta, float direction)
{
	pos.y+= direction * tDelta * velocity;
	Thetax += 2 * direction * tDelta;
	if (Thetax <= gu_radian * -25) {
		Thetax = gu_radian * -25;
	}

	if (Thetax >= gu_radian * 25) {
		Thetax = gu_radian * 25;
	}

	if (direction == 0) {
		if (Thetax < 0) {
			Thetax += 0.5*tDelta;
		}
		if (Thetax > 0) {
			Thetax -= 0.5*tDelta;
		}
	}
	calculateDerivedMatices();
}

void Vehicle::horizontal(float tDelta, float direction)
{
	pos += vec3(

		cosf(Theta) * direction * tDelta * velocity,
		0.0f,
		-sinf(Theta) * direction  * tDelta * velocity);
	calculateDerivedMatices();
}

void Vehicle::rotate(float tDelta, float direction)
{
	Theta = std::fmodf(Theta +  direction * tDelta, gu_pi * 2.0f * velocity);
	Thetaz += 2*direction * tDelta;

	if (Thetaz <= gu_radian * -25) {
		Thetaz = gu_radian * -25;
	}

	if (Thetaz >= gu_radian * 25) {
		Thetaz = gu_radian * 25;
	}
	if (direction == 0) {
		if (Thetaz < 0) {
			Thetaz += 0.5*tDelta;
		}
		if (Thetaz > 0) {
			Thetaz -= 0.5*tDelta;
		}
	}

	calculateDerivedMatices();
}

void Vehicle::render(void)
{
	glBindTexture(GL_TEXTURE_2D, Texture);
	glEnable(GL_TEXTURE_2D);
	p = T*R* mat4::translationMatrix(0, 1, 3)*vec4(0, 0, 0, 1.0);
	Position = glm::vec3(p.x, p.y, p.z);
	Target = glm::vec3(pos.x, pos.y + 0.8, pos.z);
	Up = glm::vec3(0.0f, 1.0f, 0.0f);
	camera = new Camera(Position, Target, Up);
	view = camera->getView();

	glUseProgram(Shader);

	glUniformMatrix4fv(locT, 1, GL_FALSE, (GLfloat*)&TR);
	glUniformMatrix4fv(locP, 1, GL_FALSE, (GLfloat*)&projection);
	glUniformMatrix4fv(locV, 1, GL_FALSE, (GLfloat*)&view);
	glUniform3fv(locViewPos, 1, (GLfloat*)&Position);

	for (int m = 0; m < numMeshes; m++) {
		glBindVertexArray(VAO[m]);
		glDrawElements(GL_TRIANGLES, numfaces[m] * 3, GL_UNSIGNED_INT, (GLvoid*)0);
	}

}

vec3 Vehicle::getPos()
{
	return pos;
}

float Vehicle::getTheta()
{
	return Theta;
}
