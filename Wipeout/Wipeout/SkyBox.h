#pragma once
#include"CubemapTexture.h"
#include "vehicle.h"
class SkyBox
{
	CubemapTexture *Texture;
	mat4 projection;
	vec3			pos;
	mat4 T;
	
	GLuint Shader, skyboxVAO, skyboxVBO, locP, locV, locT;
public:
	SkyBox(float X, float Y, float Z,
		const char * PosXFilename,
		const char * NegXFilename,
		const char * PosYFilename,
		const char * NegYFilename,
		const char * PosZFilename,
		const char * NegZFilename);
	~SkyBox();
	
	void Render();
};

